﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RabbitMQApp
{
    class Program
    {

        const string queueName = "myqueue";
        static void Main(string[] args)
        {
            AsyncWaitCls operation = new AsyncWaitCls();

            // string result = operation.RunSlowOperation();
            //Task<string> result = operation.RunSlowOperationTask();
            Task<string> result = operation.RunSlowOperationTaskAsync();
            operation.RunTrivialOperation();

            Console.WriteLine("Return value of slow operation: {0}", result);
            Console.WriteLine("The main thread has run complete on thread number {0}", Thread.CurrentThread.ManagedThreadId);
            Console.ReadLine();
            // InputDetails();
        }

        private static void InputDetails()
        {
            Console.WriteLine("------------------------------------- ");
            Console.WriteLine("Type Publish/Consume/Exit message");
            string typeOfMessage = Console.ReadLine();

            if (typeOfMessage == "Publish")
            {
                PublishMessage();
            }
            else if (typeOfMessage == "Consume")
            {
                ConsumeMessage();
            }
            else
            {
                //Console.WriteLine(" Press [enter] to exit.");
                //Console.Read();
                Environment.Exit(-1);
            }
        }

        private static void PublishMessage()
        {
            Console.WriteLine("Enter message to publish: ");
            string message = Console.ReadLine();

            try
            {
                var connectionFactory = new ConnectionFactory()
                {
                    HostName = "localhost",
                    UserName = "guest",
                    Password = "guest",
                    Port = 5672
                    //RequestedConnectionTimeout = 3000, // milliseconds
                };

                using (var rabbitConnection = connectionFactory.CreateConnection())
                {
                    using (var channel = rabbitConnection.CreateModel())
                    {
                        // Declaring a queue is idempotent 
                        channel.QueueDeclare(
                            queue: queueName,
                            durable: true,
                            exclusive: false,
                            autoDelete: false,
                            arguments: null);

                        string body = message; // $"A nice random message: {DateTime.Now.Ticks}";
                        channel.BasicPublish(
                            exchange: "myexchange",
                            routingKey: queueName,
                            basicProperties: null,
                            body: Encoding.UTF8.GetBytes(body));

                        Console.WriteLine("Message sent");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.ToString());
                Console.ForegroundColor = ConsoleColor.White;
            }

            // Console.WriteLine("End");
            // Console.Read();
            InputDetails();
        }

        private static void ConsumeMessage()
        {
            var connectionFactory = new ConnectionFactory()
            {
                HostName = "localhost",
                UserName = "guest",
                Password = "guest",
                Port = 5672
                //RequestedConnectionTimeout = 3000, // milliseconds
            };

            using (var rabbitConnection = connectionFactory.CreateConnection())
            {
                using (var channel = rabbitConnection.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: "myexchange", type: ExchangeType.Fanout, durable: true);

                    var queueName = "myqueue"; // channel.QueueDeclare().QueueName;
                    channel.QueueBind(queue: queueName,
                                      exchange: "myexchange",
                                      routingKey: "");

                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (model, ea) =>
                    {
                        var body = ea.Body.ToArray();
                        var message = Encoding.UTF8.GetString(body);
                        Console.WriteLine("{0}", message);
                    };
                    channel.BasicConsume(queue: queueName,
                                         autoAck: true,
                                         consumer: consumer);

                    Console.WriteLine(" Press [enter] to exit.");
                    Console.ReadLine();
                    //InputDetails();
                }
            }
        }

    }
}
